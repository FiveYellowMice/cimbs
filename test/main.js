'use strict'

const assert = require('chai').assert
const path = require('path')
const childProcess = require('child_process')

describe('cimbs', function() {
  it('should run', function(done) {
    childProcess.execFile(path.join(__dirname, '../bin/cimbs'), ['-v'])
    .on('error', (err) => {
      done(err)
    })
    .on('exit', (code) => {
      if (code === 0) {
        done()
      } else {
        done(new Error(`process exited with status ${code}`))
      }
    })
  })
})
